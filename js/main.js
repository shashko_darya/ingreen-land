var services1 = new Swiper('.flowers .swiper-container', {
    nextButton: '.flowers .swiper-button-next',
    prevButton: '.flowers .swiper-button-prev',
    slidesPerView: 4,
    slidesPerGroup: 4,
    spaceBetween: 20,
    breakpoints: {
        480: {
            slidesPerView: 1,
            slidesPerGroup: 1
        },
        668: {
            slidesPerView: 2,
            slidesPerGroup: 2
        },
        991: {
            slidesPerView: 3,
            slidesPerGroup: 3
        }
    }
});
var services2 = new Swiper('.decoration .swiper-container', {
    nextButton: '.decoration .swiper-button-next',
    prevButton: '.decoration .swiper-button-prev',
    slidesPerView: 4,
    slidesPerGroup: 4,
    spaceBetween: 20,
    breakpoints: {
        480: {
            slidesPerView: 1,
            slidesPerGroup: 1
        },
        668: {
            slidesPerView: 2,
            slidesPerGroup: 2
        },
        991: {
            slidesPerView: 3,
            slidesPerGroup: 3
        }
    }
});
var design = new Swiper('.design .swiper-container', {
    nextButton: '.design .swiper-button-next',
    prevButton: '.design .swiper-button-prev',
    slidesPerColumn: 2,
    slidesPerView: 4,
    slidesPerGroup: 4,
    spaceBetween: 20,
    breakpoints: {
        480: {
            slidesPerView: 1,
            slidesPerGroup: 1
        },
        668: {
            slidesPerView: 2,
            slidesPerGroup: 2
        },
        991: {
            slidesPerView: 3,
            slidesPerGroup: 3
        }
    }
});
var reviews = new Swiper('.reviews .swiper-container', {
    nextButton: '.reviews .swiper-button-next',
    prevButton: '.reviews .swiper-button-prev',
    slidesPerView: 3,
    slidesPerGroup: 1,
    spaceBetween: 20,
    breakpoints: {
        480: {
            slidesPerView: 1,
            slidesPerGroup: 1
        },
        668: {
            slidesPerView: 2,
            slidesPerGroup: 2
        },
        991: {
            slidesPerView: 3,
            slidesPerGroup: 3
        }
    }
});
var clients = new Swiper('.clients .swiper-container', {
    nextButton: '.clients .swiper-button-next',
    prevButton: '.clients .swiper-button-prev',
    slidesPerColumn: 2,
    slidesPerView: 4,
    slidesPerGroup: 4,
    spaceBetween: 0,
    breakpoints: {
        480: {
            slidesPerView: 1,
            slidesPerGroup: 1
        },
        668: {
            slidesPerView: 2,
            slidesPerGroup: 2
        },
        991: {
            slidesPerView: 3,
            slidesPerGroup: 3
        }
    }
});